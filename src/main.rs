extern crate fern;
extern crate log;
extern crate serde_json;

use log::info;
use serde_json::json;

fn main() -> Result<(), Box<std::error::Error>> {
    setup_logger()?;
    info!("Starting up");
    println!("Hello, world!");
    let _x = 1;
    Ok(())
}

fn setup_logger() -> Result<(), log::SetLoggerError> {
    fern::Dispatch::new()
        .format(|out, message, record| {
            let msg = json!({
                "time": chrono::Local::now().format("[%Y-%m-%d %H:%M:%S]").to_string(),
                "level": record.level().to_string(),
                "message": message
            });
            out.finish(format_args!("{}", msg))
        })
        .chain(std::io::stdout())
        .apply()?;
    Ok(())
}
