FROM ocaml/opam2:debian-stable

RUN sudo apt-get update
RUN sudo apt-get install --yes m4 
RUN sudo apt-get clean
RUN sudo apt-get autoclean
RUN opam repo set-url default https://opam.ocaml.org 
RUN opam update
RUN opam upgrade
RUN opam install --yes "dune>=1.8" "core>=0.12" "async>=0.12"
RUN opam clean